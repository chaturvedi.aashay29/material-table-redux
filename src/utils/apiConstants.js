export const api = {
  baseUrl: process.env.REACT_APP_API_URL + "/",
  getAllLeads: "api/leads/?location_string=",
  addDeleteLead: "api/leads/",
  saveComm: "api/mark_lead/",
};
