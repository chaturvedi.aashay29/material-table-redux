import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { getLeads, addLead, saveComm, deleteLead } from "./reduxFiles/actions";
import Button from "@material-ui/core/Button";
import MaterialTable from "material-table";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import Clear from "@material-ui/icons/Clear";
import Search from "@material-ui/icons/Search";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Check from "@material-ui/icons/Check";
import FilterList from "@material-ui/icons/FilterList";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Save from "@material-ui/icons/Save";
import Remove from "@material-ui/icons/Remove";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import CloseIcon from "@material-ui/icons/Close";
import Modal from "react-bootstrap/Modal";
import TextField from "@material-ui/core/TextField";
import InputBase from "@material-ui/core/InputBase";
import { withStyles } from "@material-ui/core/styles";
import "./App.css";

// height of the TextField
const height = 35;

const mapStateToProps = (state) => ({
  allLeads: state.app.allLeads,
  closeModal: state.app.closeModal,
});

const actionCreators = {
  getLeads,
  addLead,
  saveComm,
  deleteLead,
};

const App = (props) => {
  const { getLeads, allLeads, addLead, saveComm, deleteLead } = props;

  const [openAddLead, setOpenAddLead] = useState(false);
  const [openDeleteLead, setOpenDeleteLead] = useState(false);
  const [openUpdateComm, setOpenUpdateComm] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [emailAddress, setEmailAddress] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [locationType, setLocationType] = useState("");
  const [locationString, setLocationString] = useState("");
  const [commText, setCommText] = useState("");
  const [leadID, setLeadID] = useState(0);

  const [tableColumns, setTableColumns] = useState({
    columns: [
      {
        title: "Name",
        field: "name",
        render: (rowData) => (
          <span>
            {rowData.first_name} {rowData.last_name}
          </span>
        ),
      },
      {
        title: "Email",
        field: "email",
      },
      {
        title: "Mobile Num",
        field: "mobile",
      },
      {
        title: "Location Type",
        field: "location_type",
      },
      {
        title: "Location String",
        field: "location_string",
      },
    ],
  });

  useEffect(() => {
    getLeads("India");
  }, []);

  const actions = () => {
    return [
      {
        icon: () => (
          <span
            className="update_lead_modal_btn"
            style={{
              fontSize: "15px",
              backgroundColor: "black",
              color: "white",
              border: "1px solid black",
              borderRadius: "5px",
              padding: 5,
            }}
          >
            Update
          </span>
        ),
        tooltip: "Update Column",
        onClick: (event, row) => {
          setOpenUpdateComm(true);
          setLeadID(row.id);
          setCommTextInBox(row.id);
        },
      },

      {
        icon: () => (
          <span
            className="delete_lead_modal_btn"
            style={{
              fontSize: "15px",
              backgroundColor: "black",
              color: "white",
              border: "1px solid black",
              borderRadius: "5px",
              padding: 5,
            }}
          >
            Delete
          </span>
        ),
        tooltip: "Delete Column",
        onClick: (event, row) => {
          setOpenDeleteLead(true);
          setLeadID(row.id);
        },
      },
    ];
  };

  const setCommTextInBox = (id) => {
    let obj = allLeads.find((o) => o.id === id);

    setCommText(obj.communication);
  };

  const saveLeadData = () => {
    let data = {
      first_name: firstName,
      last_name: lastName,
      mobile: mobileNumber,
      email: emailAddress,
      location_string: locationString,
      location_type: locationType,
    };
    addLead(data).then((result) => {
      if (result.statusText == "Created") {
        setOpenAddLead(false);
        getLeads("India");
      }
    });
  };

  const saveLeadComm = () => {
    let data = {
      communication: commText,
    };

    saveComm(leadID, data).then((result) => {
      if (result.statusText == "Accepted") {
        setOpenUpdateComm(false);
        getLeads("India");
      }
    });
  };

  const deleteLeadData = () => {
    deleteLead(leadID).then((result) => {
      if (result.statusText == "No Content") {
        setOpenDeleteLead(false);
        getLeads("India");
      }
    });
  };

  return (
    <div className="m-3">
      <span
        style={{
          backgroundColor: "black",
          color: "white",
          border: "1px solid black",
          borderRadius: "5px",
        }}
        className="p-2 cursor-pointer add_lead_modal_btn"
        onClick={() => setOpenAddLead(true)}
      >
        Add Lead
      </span>
      <div className="my-3 MuiToolbar-regular uniqueName leads_table">
        <MaterialTable
          options={{
            actionsColumnIndex: -1,
            showTitle: false,
            search: false,
            sorting: false,
            headerStyle: {
              backgroundColor: "black",
              color: "white",
            },
          }}
          columns={tableColumns.columns}
          key={allLeads.id}
          data={allLeads}
          icons={{
            Check: Check,
            Clear: Clear,
            DetailPanel: ChevronRight,
            Export: SaveAlt,
            Filter: FilterList,
            FirstPage: FirstPage,
            LastPage: LastPage,
            NextPage: ChevronRight,
            PreviousPage: ChevronLeft,
            Search: Search,
            ThirdStateCheck: Remove,
            Edit: Edit,
            Delete: Delete,
            SaveAlt: SaveAlt,
            Save: Save,
            SortArrow: ArrowDownwardIcon,
          }}
          actions={actions()}
        />
      </div>

      <Modal show={openAddLead}>
        <Modal.Header
          style={{
            backgroundColor: "black",
            color: "white",
            border: "1px solid black",
            alignItems: "center",
          }}
        >
          <Modal.Title>Add Lead</Modal.Title>
          <CloseIcon
            className="ml-auto cursor-pointer"
            onClick={() => setOpenAddLead(false)}
          />
        </Modal.Header>

        <Modal.Body class="add_lead_form m-3">
          <Row className="m-0 no-gutters">
            <Col className="mr-3">
              <span className="required-field">First Name</span>
              <TextField
                name="first_name"
                id="outlined-basic"
                variant="outlined"
                onChange={(e) => setFirstName(e.target.value)}
                value={firstName}
                className="my-2 w-100"
                /* styles the input component */
                inputProps={{
                  style: {
                    height,
                    padding: "0 14px",
                  },
                }}
              />
              <span className="required-field">Email Address</span>
              <TextField
                name="email"
                id="outlined-basic"
                variant="outlined"
                onChange={(e) => setEmailAddress(e.target.value)}
                value={emailAddress}
                className="my-2 w-100"
                /* styles the input component */
                inputProps={{
                  style: {
                    height,
                    padding: "0 14px",
                  },
                }}
              />
              <span className="required-field">Location Type</span>
              <br />
              <select
                name="location_type"
                className="my-2 w-100 px-2 pt-1 pb-2"
                style={{ border: "1px solid #e0e0e0", borderRadius: "3px" }}
                value={locationType}
                onChange={(e) => {
                  setLocationType(e.target.value);
                }}
              >
                <option key="1" value="City">
                  City
                </option>
                <option key="2" value="Country">
                  Country
                </option>
              </select>
            </Col>
            <Col className="ml-2">
              <span className="required-field">Last Name</span>
              <TextField
                name="last_name"
                id="outlined-basic"
                variant="outlined"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
                className="my-2 w-100"
                /* styles the input component */
                inputProps={{
                  style: {
                    height,
                    padding: "0 14px",
                  },
                }}
              />
              <span className="required-field">Mobile</span>
              <TextField
                name="mobile"
                id="outlined-basic"
                variant="outlined"
                onChange={(e) => setMobileNumber(e.target.value)}
                value={mobileNumber}
                className="my-2 w-100"
                /* styles the input component */
                inputProps={{
                  style: {
                    height,
                    padding: "0 14px",
                  },
                }}
              />
              <span className="required-field">Location String</span>
              <TextField
                name="location_string"
                id="outlined-basic"
                variant="outlined"
                onChange={(e) => setLocationString(e.target.value)}
                value={locationString}
                className="my-2 w-100"
                /* styles the input component */
                inputProps={{
                  style: {
                    height,
                    padding: "0 14px",
                  },
                }}
              />
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={() => setOpenAddLead(false)}>
            Close
          </Button>
          <Button
            className="add_lead_btn"
            style={{ backgroundColor: "transparent", border: 0 }}
            variant="primary"
            onClick={() => saveLeadData()}
          >
            Save
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={openUpdateComm}>
        <Modal.Header
          style={{
            backgroundColor: "black",
            color: "white",
            border: "1px solid black",
            alignItems: "center",
          }}
        >
          <Modal.Title>Mark Communication</Modal.Title>
          <CloseIcon
            className="ml-auto cursor-pointer"
            onClick={() => setOpenUpdateComm(false)}
          />
        </Modal.Header>

        <Modal.Body class="update_lead_form m-3">
          <span>Communication</span>
          <br />
          <textarea
            name="communication"
            rows="4"
            className="w-100 mt-2"
            onChange={(e) => setCommText(e.target.value)}
          >
            {commText}
          </textarea>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={() => setOpenUpdateComm(false)}>
            Close
          </Button>
          <Button
            className="update_lead_btn"
            style={{ backgroundColor: "transparent", border: 0 }}
            variant="primary"
            onClick={() => saveLeadComm()}
          >
            Save
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={openDeleteLead}>
        <Modal.Header
          style={{
            backgroundColor: "black",
            color: "white",
            border: "1px solid black",
            alignItems: "center",
          }}
        >
          <Modal.Title>Do you wish to delete this lead?</Modal.Title>
          <CloseIcon
            className="ml-auto cursor-pointer"
            onClick={() => setOpenDeleteLead(false)}
          />
        </Modal.Header>

        <Modal.Body class="delete_lead_form">
          <Row className="mx-0 my-2 justify-content-center">
            <Button
              className="delete_lead_btn"
              style={{
                backgroundColor: "red",
                color: "white",
                border: "1px solid red",
                borderRadius: "3px",
              }}
              onClick={() => deleteLeadData()}
            >
              Delete
            </Button>
            <Button
              className="ml-3"
              style={{
                backgroundColor: "black",
                color: "white",
                border: "1px solid black",
                borderRadius: "3px",
              }}
              onClick={() => setOpenDeleteLead(false)}
            >
              Cancel
            </Button>
          </Row>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default connect(mapStateToProps, actionCreators)(App);
