import { get, post, put, remove } from "../../utils/fetch";
import { api } from "../../utils/apiConstants";
import * as actionConstants from "../../utils/actionConstants";

export const getLeads = (location_string) => (dispatch, getState) => {
  get({
    url: api.baseUrl + api.getAllLeads + location_string,
  })(dispatch, getState)
    .then((response) => {
      dispatch({
        type: actionConstants.LEAD_LIST,
        payload: response.data,
      });
    })
    .catch((error) => {
      console.log("Error = ", error);
    });
};

export const addLead = (lead_data) => {
  return (dispatch, getState) => {
    return new Promise((resolve, rej) => {
      post({
        url: api.baseUrl + api.addDeleteLead,
        data: lead_data,
      })(dispatch, getState)
        .then((response) => {
          if (response.statusText == "Created") {
            resolve(response);
          }
        })
        .catch((error) => {
          console.log("Error = ", error);
        });
    });
  };
};

export const saveComm = (lead_id, comm_data) => {
  return (dispatch, getState) => {
    return new Promise((resolve, rej) => {
      put({
        url: api.baseUrl + api.saveComm + lead_id,
        data: comm_data,
      })(dispatch, getState)
        .then((response) => {
          if (response.statusText == "Accepted") {
            resolve(response);
          }
        })
        .catch((error) => {
          console.log("Error = ", error);
        });
    });
  };
};

export const deleteLead = (lead_id) => {
  return (dispatch, getState) => {
    return new Promise((resolve, rej) => {
      remove({
        url: api.baseUrl + api.addDeleteLead + lead_id,
      })(dispatch, getState)
        .then((response) => {
          if (response.statusText == "No Content") {
            resolve(response);
          }
        })
        .catch((error) => {
          console.log("Error = ", error);
        });
    });
  };
};
