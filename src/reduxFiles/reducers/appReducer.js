import * as actionConstants from "../../utils/actionConstants";

const INITIAL_STATE = {
  allLeads: [
    {
      id: 123,
      first_name: "Aashay",
      last_name: "Chaturvedi",
      location_type: "Country",
      location_string: "India",
      email: "aashay@gmail.com",
      mobile: "1234567890",
      communication: null,
    },
  ],
  closeModal: false,
};

export const AppReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionConstants.LEAD_LIST:
      return { ...state, allLeads: action.payload };
    case actionConstants.CLOSE_MODAL:
      return { ...state, closeModal: true };
    default:
      return state;
  }
};
