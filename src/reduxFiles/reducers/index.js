import { combineReducers } from "redux";
import { AppReducer as app } from "./appReducer";

const appReducer = combineReducers({
  app,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
